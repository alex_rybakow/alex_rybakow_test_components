import React from 'react';
import './button.css';

interface ButtonProps {
  text?: string,
  backgroundColor?: string,
  isAdmin?: boolean,
  onClick?: () => void
}

export const Button = ({ text, backgroundColor, isAdmin,...props }: ButtonProps) => {
  
  const toggle = isAdmin ? 'admin-button' : 'user-button';

  const styles = ['button', toggle].join(' ')

  return (
    <button
      type="button"
      className={styles}
      style={{ backgroundColor }}
      {...props}
    >
      {text}
    </button>
  );
};


