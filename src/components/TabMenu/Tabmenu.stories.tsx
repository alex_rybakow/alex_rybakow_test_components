import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { TabMenu } from './TabMenu';

export default {
  title: 'Components/TabMenu',
  component: TabMenu,
} as ComponentMeta<typeof TabMenu>;

const Template: ComponentStory<typeof TabMenu> = (args) => <TabMenu {...args} />;

export const MovieQuotes = Template.bind({});

MovieQuotes.args = {
    title: ['Micky','Ray','Fletcher'],
    text:['When lion hungry he eats','You are too clever to blackmail us','Play a game with me, Ray']
}