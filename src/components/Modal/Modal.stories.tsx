import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Modal } from './Modal';

export default {
  title: 'Components/Modal',
  component: Modal,
  args: { 
    children: (
    <form action="/" method="POST">
      <label htmlFor="email">Email:</label>
      <input type="email" id="email" name="email" />
      <button type="submit">Sign In</button>
    </form>
  )}
} as ComponentMeta<typeof Modal>;

const Template: ComponentStory<typeof Modal> = (args) => <Modal {...args} />;

export const PrimaryModal = Template.bind({});

PrimaryModal.args = {
active: true,
 }
