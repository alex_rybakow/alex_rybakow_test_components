import React, { HTMLAttributes } from 'react';

export interface ModalProps extends HTMLAttributes<HTMLDivElement> {
    active?: boolean;
    setActive?: (active: boolean) => void;
    children?: React.ReactNode,
  };

  export const Modal = ({ active, setActive = () => {}, children }:ModalProps) => {

    if (!active) {
      return null;
    }
    return (
      <div className={active ? "modal active" : "modal"} onClick={() => setActive(false)}>
        <div className="modal-content" onClick={(event) => event.stopPropagation()}>
          {children}
        </div>
      </div>
    )
  }
