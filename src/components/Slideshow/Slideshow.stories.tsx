import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import Slideshow  from './Slideshow';

export default {
  title: 'Components/Slideshow',
  component: Slideshow,
} as ComponentMeta<typeof Slideshow>;

const Template: ComponentStory<typeof Slideshow> = (args) => <Slideshow {...args} />;

export const ClickableSlides = Template.bind({});

ClickableSlides.args = {
  slides: ['Here', 'We','Go','Storybook'], 
  backgroundColor: 'coral'
};

