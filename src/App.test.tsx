import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import  Modal  from './App'
import userEvent from "@testing-library/user-event"

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/Counter button/i);
  expect(linkElement).toBeInTheDocument();
});

test("show second slide after the click", () => {
  render(<App/>)
  const button = screen.getByTestId('counter');
  
  userEvent.click(button)
  expect(button).toHaveTextContent('1');
})

test("shows modal inputs", () => {
  render(<Modal/>)
  
  const input = screen.getByTestId('input-name');
  
  expect(input).toBeInTheDocument();
})









