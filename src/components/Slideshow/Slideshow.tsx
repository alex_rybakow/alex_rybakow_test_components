import React, { useState } from "react";
import './slideshow.css'

interface SlideShowProps {
  slides?: string[],
  backgroundColor?: string
}

const Slideshow = ( {slides = ['slides'], backgroundColor }: SlideShowProps) => {
  let [slide, setSlide] = useState(0);

  return (
    <div
      className="container"
      onClick={() => 
        setSlide((slide + 1) % slides.length)}
    >
      {slides.map((text, i) => (
        <div
          className="slide"
          style={{ backgroundColor }}
          key={i}
          hidden={i !== slide || undefined}
        >
          {text}
        </div>
      ))}
    </div>
  );
}

export default Slideshow;