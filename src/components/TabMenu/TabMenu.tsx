import React, { useState} from 'react';
import './tabmenu.css';

interface TabmenuProps {
  title?: string[],
  text?: string[],
}

export const TabMenu = ({title = ['Title'], text = ['Text'] } : TabmenuProps) => {
    const [tab, setTab] = useState<number>(1);
    const toggle = (index: number) => setTab(index);
     
    
    const firstTitle = tab === 1 ? "tabs active-tabs" : "tabs"
    const secondTitle = tab === 2 ? "tabs active-tabs" : "tabs"
    const thirdTitle = tab === 3 ? "tabs active-tabs" : "tabs"

    const firstTab = tab === 1 ? "content active-content" : "content"
    const secondTab = tab === 2 ? "content active-content" : "content"
    const thirdTab = tab === 3 ? "content active-content" : "content"


    return(
        <div className="container">
            <div className="titles">
            <button className={firstTitle}
            data-testid="first-title"
             onClick = {() => toggle(1)}
            >
            {title[0]}
            </button>
            <button className={secondTitle}
            data-testid="second-title"
             onClick = {() => toggle(2)}
            >
            {title[1]}
            </button>
            <button className={thirdTitle}
            data-testid="third-title"
             onClick = {() => toggle(3)}
            >
            {title[2]}
            </button>
            </div>
            <div className="contents">
            <button className={firstTab}
            data-testid="first-tab"
            >
            {text[0]}
            </button>
            <button className={secondTab}
            data-testid="second-tab"
            >
            {text[1]}
            </button>
            <button className={thirdTab}
            data-testid="third-tab"
            >
            {text[2]}
            </button>
            </div>
        </div>
    )

}

