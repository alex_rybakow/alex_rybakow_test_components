import React from 'react';
import { MovieQuotes } from './Tabmenu.stories'
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";


test('should open tab when clicked', () => {
    render(<MovieQuotes {...MovieQuotes.args} />);
  const secondTitle = screen.getByTestId("second-title");

  userEvent.click(secondTitle)
  expect(screen.getByTestId("second-tab")).toHaveTextContent('You are too clever to blackmail us');
    
  });

  test('check styles on clicked tabs', () => {
    render(<MovieQuotes {...MovieQuotes.args} />);
  const thirdTitle = screen.getByTestId("third-title");

  userEvent.click(thirdTitle)
  expect(screen.getByTestId("third-tab")).toHaveStyle("display: inline-block");
    
  });

  