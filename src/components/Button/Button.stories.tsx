import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Button } from './Button';

export default {
  title: 'Components/Button',
  component: Button,
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;

export const AdminButton = Template.bind({});

AdminButton.args = {
  isAdmin: true,
  text: 'Admin',
  onClick: () => alert('Welcome to the admin page')
};

export const UserButton = Template.bind({});
UserButton.args = {
  isAdmin: false,
  text: 'User',
  onClick: () => alert('Welcome to the user page')
};

export const UserColor = Template.bind({});
UserColor.args = {
  text: 'User Color',
  backgroundColor: "crimson",
  onClick: () => alert('Choose your color')
};