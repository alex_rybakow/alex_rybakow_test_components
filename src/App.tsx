import React, { useState } from 'react';
import './App.css';
import TrumpImage from './assets/trump.jpeg';
import { Modal } from './components/Modal/Modal'


const App = () => {
  const [counter, setCounter] = useState(0)
  const [modalActive, setModalActive] = useState(false)
  const [tweetModalActive, settweetModalActive] = useState(false)
  const [thankYouModalActive, setThankYouModalActive] = useState(false)
  const [errorModalActive, setErrorModalActive] = useState(false)
  const [username, setUserName] = useState(' ')
  const [nickname, setNickname] = useState(' ')
  const [age, setAge] = useState(' ')
  const [character, setCharacter] = useState(' ')

  const nameChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
    setUserName(event.target.value)
  }

  const nickNameChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
    setNickname(event.target.value)
  }

  const ageChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
    setAge(event.target.value)
  }

  const characterChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCharacter(event.target.value)
  }

  const checkInput = () => {
    if (username.length < 3 || nickname.length < 3 || age.length < 1 || character.length < 3) {
      setErrorModalActive(true)
    } else setThankYouModalActive(true)
  };

  const handleCount = () => {
    if (counter < 3) {
      setCounter(counter + 1)
    } else settweetModalActive(true)
  }

  const ShowTweet = () => {
    if (counter === 3) {
      return (
        <Modal active={tweetModalActive} setActive={settweetModalActive}><img src={TrumpImage} alt="logo" className="image" /> </Modal>
      );
    } else return null;
  }

  const ShowError = () => {
    setTimeout(() => {
      setErrorModalActive(false)
    },2000)
    return (
      <Modal active={errorModalActive} setActive={setErrorModalActive}><p className="text">Пожалуйста, заполните корректно все поля</p> </Modal>
    );
  }

  const ShowThankYou = () => {
    setTimeout(() => {
      setThankYouModalActive(false)
    },2000)
    return (
      <Modal active={thankYouModalActive} setActive={setThankYouModalActive}><p className="text">Ваши ответы были получены, спасибо</p> </Modal>
    );
  }

  return (
    <div className="App">
      <div>
        <h3>Modal button</h3>
        <button className="button" onClick={() => setModalActive(true)}>
          Заполнить форму
      </button>
        <Modal active={modalActive} setActive={setModalActive}>
        <img src="https://d30y9cdsu7xlg0.cloudfront.net/png/53504-200.png" alt="close-btn" className="close-button" onClick={() => setModalActive(false)}/>
          <form>
          <div className="input-block">
            <label htmlFor="username">Имя</label>
            <input type='text' className='input' id='name-input' value={username} onChange={nameChanged}/></div>
          <div className="input-block">
            <label htmlFor="nickname">Фамилия</label>
            <input type="text" className="input" name="Никнейм" aria-label="Default" aria-describedby="inputGroup-sizing-default" value={nickname} onChange={nickNameChanged} /></div>
          <div className="input-block">
            <label htmlFor="age">Возраст</label>
            <input type="number" className="input" name="Возраст" value={age} onChange={ageChanged} /></div>
          <div className="input-block">
            <label htmlFor="character">Тот, чье имя нельзя называть</label>
            <input type="text" className="input" name="Тот, чье имя нельзя называть" value={character} onChange={characterChanged} /></div>
          <button type="button" className="button modal-button" onClick={checkInput}>Отправить</button>
          </form>
          <ShowThankYou />
          <ShowError />
        </Modal>
      </div>
      <div>
        <h3>Counter button</h3>
        <button className="button" data-testid="counter" onClick={handleCount}>
          {counter}
        </button>
        <ShowTweet />
      </div>
    </div>
  );
}

export default App;
