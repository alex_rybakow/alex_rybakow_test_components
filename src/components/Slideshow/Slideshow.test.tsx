import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import userEvent from "@testing-library/user-event"

import { ClickableSlides }  from './Slideshow.stories';

test("first slide is seen by default", () => {
    render(<ClickableSlides {...ClickableSlides.args} />)
    const first = screen.getByText('Here');
    const second = screen.getByText('We');
    const third = screen.getByText('Go');
    const fourth = screen.getByText('Storybook')
    
    expect(first).toBeVisible();
    expect(second).not.toBeVisible();
    expect(third).not.toBeVisible();
    expect(fourth).not.toBeVisible()
})

test("show second slide after the click", () => {
    render(<ClickableSlides {...ClickableSlides.args} />)
    const first = screen.getByText('Here');
    
    userEvent.click(first)
    expect(screen.getByText('We')).toBeVisible();
})

test("check the color of the slide", () => {
    render(<ClickableSlides {...ClickableSlides.args} />)
    const first = screen.getByText('Here');

    expect(first).toHaveStyle('background-color:coral');
})