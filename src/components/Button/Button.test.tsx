import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import { AdminButton, UserColor } from './Button.stories';

test('renders the button for the admin page', () => {
  render(<AdminButton {...AdminButton.args} />);
  expect(screen.getByRole('button')).toHaveTextContent('Admin');
});

test('renders the button for user color choice', () => {
    render(<UserColor {...UserColor.args} />);
    expect(screen.getByRole('button')).toHaveTextContent('User Color');
    expect(screen.getByRole('button')).toHaveStyle("background-color: crimson")
  });
